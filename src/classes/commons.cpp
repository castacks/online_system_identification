#include <sysid/commons.h>

namespace sysid
{
    bool DEBUG = false;
    double variance_fault_threshold = 5.0F;
    double reset_after_seconds = 3.0F;
    
} // end namespace sysid

