# Online System and Identification and Anomaly Detection
This repository monitors the input/output signals from the autopilot to identify the model of the system and detect anomalies in autonomous flights.

## Dates of work
April 2017 - October 2018

## Instructions
The guide to build and run the code is available in the [Wiki](https://bitbucket.org/castacks/online_system_identification/wiki/Home "Wiki").

## Copyright
Copyright (c) 2018 Carnegie Mellon University  
This software was developed under a NASA contract NNX17CS56C-01.

## Author
Azarakhsh Keipour (CMU Ph.D. Student): keipour@cmu.edu
