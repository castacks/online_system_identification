/*
* Copyright (c) 2018 Carnegie Mellon University, Azarakhsh Keipour <akeipour@andrew.cmu.edu>
* This software was developed under a NASA contract NNX17CS56C-01.
*
*/

#ifndef __SYSID_MATRIX_OPERATIONS_INCLUDED__
#define __SYSID_MATRIX_OPERATIONS_INCLUDED__

#include <sysid/commons.h>
#include <cstddef>

namespace sysid
{    
    class Matrix
    {
    private:
        std::size_t rows;
        std::size_t cols;
        DataType* data;

    public:
        ///
        /// Default constructor for the Matrix class.
        /// Initializes the Matrix with 0 rows and 0 columns.
        ///
        Matrix();

        ///
        /// Constructor for the Matrix class.
        /// Initializes the Matrix with specified number of rows and columns.
        ///
        Matrix(std::size_t nrows, std::size_t ncols);

        ///
        /// Constructor for the Matrix class.
        /// Initializes the Matrix with the single scalar data.
        /// Resulting rows and cols will be equal to 1.
        ///
        Matrix(DataType scalar);

        ///
        /// Constructor for the Matrix class.
        /// Initializes the Matrix with the input matrix size and data.
        ///
        Matrix(const Matrix & mat);

        ///
        /// Desctructor for the Matrix class.
        ///
        ~Matrix();

        ///
        /// Reset all Matrix elements to zero.
        ///
        void Reset();

        ///
        /// Returns the number of rows in the Matrix.
        ///
        std::size_t GetRows();

        ///
        /// Returns the number of columns in the Matrix.
        ///
        std::size_t GetCols();

        ///
        /// Index operator for the Matrix.
        ///
        DataType* operator[](std::size_t row) const;

        ///
        /// Add operator for the Matrix. Adds two Matrices.
        ///
        Matrix operator+(Matrix const& mat) const;

        ///
        /// Subtract operator for the Matrix. Subtracts 
        /// the right Matrix from the left Matrix.
        ///
        Matrix operator-(Matrix const& mat) const;

        ///
        /// Multiplication operator for the matrix. Multiplies two
        /// matrices.
        ///
        Matrix operator*(Matrix const& mat) const;

        ///
        /// Multiplication operator for the matrix. Multiplies a
        /// scalar value to a Matrix.
        ///
        Matrix operator*(DataType val) const;

        ///
        /// Assignment operator for the Matrix. Assigns the right-hand-side
        /// Matrix to the left Matrix.
        ///
        Matrix & operator=(const Matrix & mat);

        friend Matrix operator*(DataType val,const Matrix& mat);

        Matrix operator/(DataType val) const;

        Matrix Trans() const;

        DataType ToScalar ();

        bool IsCorrect();

        void Print();
    };
}

#endif // __SYSID_MATRIX_OPERATIONS_INCLUDED__
