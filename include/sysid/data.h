/*
* Copyright (c) 2018 Carnegie Mellon University, Azarakhsh Keipour <akeipour@andrew.cmu.edu>
* This software was developed under a NASA contract NNX17CS56C-01.
*
*/

#ifndef __SYSID_DATA_INCLUDED__
#define __SYSID_DATA_INCLUDED__ 

#include <fstream>
#include <vector>
#include <string>
#include <sysid/SignalData.h>
#include <sysid/commons.h>
#include <sysid/data_item.h>
#include <stdexcept>

namespace sysid
{
    class Data 
    {
    public:
        ///
        /// The collection of all the data items.
        ///
        std::vector<DataItem> Items;
        
        ///
        /// Insert a new data item and return the index.
        ///
        size_t Insert(DataItem _item);
        
        ///
        /// Read the data item at the given index.
        ///
        DataItem &operator[](size_t _index);
        
        ///
        /// Return the number of data items.
        ///
        size_t Count();
        
        ///
        /// Load data from a file and return the count.
        ///
        size_t LoadFromFile(std::string _filename);

        ///
        /// Save all the data to a file.
        ///
        bool SaveToFile(std::string _filename);
        
        ///
        /// Clear all the data.
        ///
        void Clear();
    };
    
} // end namespace sysid
# endif // __SYSID_DATA_INCLUDED__
