/*
* Copyright (c) 2018 Carnegie Mellon University, Azarakhsh Keipour <akeipour@andrew.cmu.edu>
* This software was developed under a NASA contract NNX17CS56C-01.
*
*/

#ifndef __SYSID_TRANSFERFUNCTION_INCLUDED__
#define __SYSID_TRANSFERFUNCTION_INCLUDED__ 

#include <sysid/commons.h>
#include <vector>
#include <iostream>
#include <string>
#include <cmath>

namespace sysid
{
    class TransferFunction 
    {
    public:
        enum Types {Discrete, Continuous} Type;
        std::size_t InputSize;
        std::size_t OutputSize;
        std::vector<DataType> InputCoeffs;
        std::vector<DataType> OutputCoeffs;
        //std::vector<std::vector<DataType> > CovarianceMatrix;
        //DataType PredictionErrorVariance;
        
    public:
        TransferFunction();
        TransferFunction(Types type, std::size_t input_size, std::size_t output_size);
        void Print();
        
    private:
        void print_continuous();
        void print_discrete();
    };            

} // end namespace sysid
# endif // __SYSID_TRANSFERFUNCTION_INCLUDED__
