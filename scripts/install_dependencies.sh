install_ws = $HOME/fixedwing_ws

sudo apt-get update
sudo apt-get install ros-kinetic-rqt ros-kinetic-roscpp ros-kinetic-std-msgs libqwt-dev libqwt-qt5-dev -y

cd $install_ws/src
git clone https://github.com/ethz-asl/variant.git
git clone https://github.com/ethz-asl/rqt_multiplot_plugin.git

cd ..
catkin build rqt_multiplot
